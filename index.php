<?php 
	include_once "includes/databasehost.php"
?>

<!DOCTYPE html>
<html>
<head>
	<title>Sales</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div>
		<h2>
			<b>Product List</b>
			<button class="button">Apply</button>
		</h2>
		<hr>
	</div>

	<div class="row">
		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
				<?php
				$sql = "SELECT * FROM product WHERE product_id = 1;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>
			</div>
		</div>

		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
			  <?php
				$sql = "SELECT * FROM product WHERE product_id = 2;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>

			</div>
		</div>

		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
			  <?php
				$sql = "SELECT * FROM product WHERE product_id = 3;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>

			</div>
		</div>

		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
			  <?php
				$sql = "SELECT * FROM product WHERE product_id = 4;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>

			</div>
		</div>

	</div>
	<div class="row">
		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
			  <?php
				$sql = "SELECT * FROM product WHERE product_id = 5;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>

			</div>
		</div>

		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
			  <?php
				$sql = "SELECT * FROM product WHERE product_id = 6;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>

			</div>
		</div>

		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
			  <?php
				$sql = "SELECT * FROM product WHERE product_id = 7;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>

			</div>
		</div>
		
		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
			  <?php
				$sql = "SELECT * FROM product WHERE product_id = 8;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>

			</div>
		</div>
	</div>

	</div>
	<div class="row">
		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
			  <?php
				$sql = "SELECT * FROM product WHERE product_id = 9;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>

			</div>
		</div>

		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
			  <?php
				$sql = "SELECT * FROM product WHERE product_id = 10;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>

			</div>
		</div>

		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
			  <?php
				$sql = "SELECT * FROM product WHERE product_id = 11;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>

			</div>
		</div>

		<div class="column">
			<div class="card">
				<div class="label">
					<label class="container">
						<input type="checkbox">
					</label>
				</div>
			  <?php
				$sql = "SELECT * FROM product WHERE product_id = 12;";
				$result = mysqli_query($conn,$sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck > 0) {
					while($row = mysqli_fetch_assoc($result)) {
						echo '<h3>'.$row['product_SKU'].'</h3>';
						echo $row['product_name'].'<br>';
						echo '<br>'.$row['product_price'].'<br>';
						echo '<br>'.$row['product_character'];
					}
				}
				?>

			</div>
		</div>

	</div>

</body>
</html>