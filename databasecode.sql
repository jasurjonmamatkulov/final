CREATE TABLE product (
	product_id int(7) AUTO_INCREMENT PRIMARY KEY not null,
	product_SKU varchar(70) not null,
	product_name varchar(70) not null,
	product_price varchar(70) not null,
	product_character varchar(70) not null
);


INSERT INTO `product`( `product_SKU`, `product_name`, `product_price`, `product_character`)
VALUES ('JVC200123', 'Acme DISC', '100 $', 'Size: 700 MB'),
('JVC200123', 'Acme DISC', '100 $', 'Size: 700 MB'),
('JVC200123', 'Acme DISC', '100 $', 'Size: 700 MB'),
('JVC200123', 'Acme DISC', '100 $', 'Size: 700 MB'),
('GGWP007', 'War and Peace', '20.00 $', 'Weight: 2 KG'),
('GGWP007', 'War and Peace', '20.00 $', 'Weight: 2 KG'),
('GGWP007', 'War and Peace', '20.00 $', 'Weight: 2 KG'),
('GGWP007', 'War and Peace', '20.00 $', 'Weight: 2 KG'),
('TR120555', 'Chair', '40.00 $', 'Dimension: 24x45x15'),
('TR120555', 'Chair', '40.00 $', 'Dimension: 24x45x15'),
('TR120555', 'Chair', '40.00 $', 'Dimension: 24x45x15'),
('TR120555', 'Chair', '40.00 $', 'Dimension: 24x45x15'),