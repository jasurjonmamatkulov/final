<!DOCTYPE html>
    <html>
    <head>
        <title>Product add</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
    	<script src="javascript.js"></script>

        <div class="PrAd">
            <h2>
                <b>Product Add</b>
                <form action="includes/created_product.php" method="GET">
                  <button class="button" type="submit" name="submit">Save</button>
                </form>
            </h2>
            <hr>
        </div>
        <div class="form">
            <form action="includes/created_product.php" method="GET">
                <label for="SKU">SKU</label>
                <input type="text" id="formbox" name="SKU"><br>
                <label for="Name">Name</label>
                <input type="text" id="formbox" name="Name"><br>
                <label for="Price">Price</label>
                <input type="number" id="formbox" name="Price"><br>
                <label>Type Switcher</label>
                <select id="type" name="product" onChange="prodType(this.value);">
                    <option value="">Type Switcher</option>
                    <option value="Acme Disc">Acme Disc</option>
                    <option value="War and Peace">War and Peace</option>
                    <option value="Chair">Chair</option>
                </select>
                
                <div class="fieldbox" id="acme_disc_attributes">
                  <label>Size</label>
                  <input type="number" name="size">
                  <p>"Please provide size in MB format"</p>
                </div>
                
                <div class="fieldbox" id="war_peace_attributes">
                  <label>Weight</label>
                  <input type="number" name="weight">
                  <p>"Please provide weight in KG format"</p>
                </div>
                
                <div class="fieldbox" id="chair_attributes">
                  <label>Height</label>
                  <input type="text" name="length"><br>
                  <label>Width</label>
                  <input type="text" name="width"><br>
                  <label>Lenght</label>
                  <input type="text" name="length"><br>
                  <p>"Please provide dimensions  in HxWxL format"</p>
                </div>
            </form>
        </div>
    </body>
    </html>